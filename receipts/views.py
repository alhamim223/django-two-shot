from django.shortcuts import render, redirect
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, AccountForm
from receipts.models import ExpenseCategory, Account

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    return render(request, "receipts/create.html", {"form": form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    return render(
        request, "receipts/category.html", {"categories": categories}
    )


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    return render(request, "receipts/account.html", {"accounts": accounts})


from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import ExpenseCategoryForm


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    return render(request, "receipts/create_category.html", {"form": form})


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    return render(request, "receipts/create_account.html", {"form": form})
